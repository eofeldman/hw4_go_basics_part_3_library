package hw4_go_basics_part_3_library

import (
	"io/ioutil"
	"strings"
)

func AnalyzeFile(filename string, countRows bool, countWords bool) (int, int, error) {
	content, err := ioutil.ReadFile(filename)
    if err != nil {
      return 0, 0, err
    }

	text := string(content)
	var nRows int 
	if countRows {
		nRows = CountRows(text)
	}

	var nWords int
	if countWords {
		nWords = CountWords(text)
	}

	return nRows, nWords, nil
}

func CountRows(text string) int {
	return strings.Count(text, "\n") + 1
}

func CountWords(text string) int {
	words := strings.Fields(text)
	return len(words)
}
